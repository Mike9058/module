//
//  Cache.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol CacheProcessingUnit <ProcessingUnit>
-(id) objectForKey:(id) key;
-(void) setObject:(id)obj forKey:(id) value;
-(void) removeObjectForKey:(id) key;
-(void) removeAllObjects;
@end

typedef NS_ENUM(NSUInteger, CacheType) {
    CacheData,
    CacheFile,
};
@interface Cache : NSObject<Module>

@end
