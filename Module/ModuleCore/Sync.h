//
//  Sync.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol  LibSongProcessingUnit<ProcessingUnit>

@end

@protocol LibAlbumProcessingUnit <ProcessingUnit>

@end

@protocol  PlayLogProcessingUnit<ProcessingUnit>

@end

@interface Sync : NSObject<Module>

@end
