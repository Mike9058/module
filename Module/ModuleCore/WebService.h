//
//  WebService.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol WebDataProcessingUnit <ProcessingUnit>
-(id) GETMethord:(NSString*) methord parameters:(NSDictionary*) parameters block:(void(^)(id result,NSError* error)) block;
@end

@protocol WebFileProcessingUnit <ProcessingUnit>
-(id) GETFileForURL:(NSURL*) url block:(void(^)(id result,NSError* error)) block progress:(void(^)(float)) progress;
@end

@interface WebService : NSObject

@end
