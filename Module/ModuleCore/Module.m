//
//  Module.m
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import "Module.h"

@implementation UIApplication (Module)
-(Module<Module>) module:(NSString*) name{
    Class module = NSClassFromString(name);
    return module;
}
@end