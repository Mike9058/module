//
//  Device.m
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import "Device.h"

@implementation Device
+(NSArray*) requires{
    return nil;
}
+(id<ProcessingUnit>) processingUnit:(NSDictionary*) info{
    static id __obj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __obj = [[self alloc] init];
    });
    return __obj;
}
@end
