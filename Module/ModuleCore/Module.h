//
//  Module.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

#define ModuleRequir(Name,Type,Model,Info) \
-(Type) Name{\
static int const t;\
@synchronized(self){\
id obj = objc_getAssociatedObject(self, &t);\
if(obj) return obj;\
obj = [[[UIApplication sharedApplication] module:@#Model] processingUnit:Info];\
objc_setAssociatedObject(self, &t, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);\
return obj;\
}\
}


@protocol ProcessingUnit <NSObject>

@end

@protocol Module <NSObject>
+(NSArray*) requires;
+(id<ProcessingUnit>) processingUnit:(NSDictionary*) info;
@end

typedef Class Module;

@interface UIApplication (Module)
-(Module<Module>) module:(NSString*) name;
@end
