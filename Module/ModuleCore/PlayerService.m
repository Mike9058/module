//
//  PlayerService.m
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import "PlayerService.h"
#import "WebService.h"
#import "Store.h"
#import "Download.h"
#import "Cache.h"

@implementation PlayerService
ModuleRequir(webService, id<WebDataProcessingUnit>, WebService, nil);
ModuleRequir(store, id<StoreProcessingUnit>, Store, nil);
ModuleRequir(cache, id<CacheProcessingUnit>, Cache, nil);
ModuleRequir(downloader, id<DownloadProcessingUnit>, Download, nil);
@end
