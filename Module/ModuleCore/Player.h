//
//  Player.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"


@protocol PlayerProcessingUnit <ProcessingUnit>

@end

@interface Player : NSObject<Module,PlayerProcessingUnit>

@end
