//
//  Download.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"



@protocol DownloadProcessingUnit <ProcessingUnit>
-(void) downloadSong:(NSUInteger) songID;
@end

@interface Download : NSObject<Module>

@end
