//
//  Device.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol DeviceProcessingUnit <ProcessingUnit>
-(unsigned long long) diskSize;
-(unsigned long long) freeDiskSize;
@end

@interface Device : NSObject<Module,DeviceProcessingUnit>

@end
