//
//  DB.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol DBProcessingUnit <ProcessingUnit>
-(NSArray *) allObjects;
-(id) objectForKey:(id) key;
-(void) setObject:(id) object forKey:(id) value;
-(void) removeObjectForKey:(id) key;
@end

typedef NS_ENUM(NSUInteger, DBType) {
    DBOffline,
    DBAlbum,
    DBUser
};
@interface DB : NSObject<Module>
@end
