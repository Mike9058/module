//
//  WebService.m
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import "WebService.h"
#import "Cache.h"

@interface WebDataUnit : NSObject <WebDataProcessingUnit>

@end
@implementation WebDataUnit
ModuleRequir(cache, id<CacheProcessingUnit>, Cache, @{@"type":@(CacheData)})
@end

@implementation WebService

@end
