//
//  Store.h
//  Module
//
//  Created by teym on 14-7-15.
//  Copyright (c) 2014年 xiami. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@protocol StoreProcessingUnit <ProcessingUnit>
-(void) saveSong:(NSUInteger)songId path:(NSString*) path;
-(NSString*) pathForSong:(NSUInteger) songId;
@end

@interface Store : NSObject<Module>

@end
